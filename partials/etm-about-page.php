<div id="etm-about" class="wrap">
	<h1 id="tab_title">
		<img id="tab_title__logo" alt="WEB-T logo" />
		<?php esc_html_e( 'WEB-T – eTranslation Multilingual About', 'etranslation-multilingual' ); ?>
	</h1>
	<?php do_action( 'etm_settings_navigation_tabs' ); ?>

	<div class="grid feat-header">
		<div class="grid-cell">
			<h2><?php esc_html_e( 'About this plugin', 'etranslation-multilingual' ); ?></h2>
			<p>
				<?php
				echo wp_kses(
					__( 'WEB-T – eTranslation Multilingual a plugin that allows you to translate your WordPress website content using machine translation or manually. It comes with a default translation provider, eTranslation, but can be configured to use custom providers.<br/>See how to <a target="_blank" href="https://website-translation.language-tools.ec.europa.eu/solutions/wordpress_en">configure and use this plugin</a>.', 'etranslation-multilingual' ),
					array(
						'br' => array(),
						'a'  => array(
							'href'   => array(),
							'target' => array(),
						),
					)
				);
				?>
			</p>

			<h3><?php esc_html_e( 'More information', 'etranslation-multilingual' ); ?></h3>
			<p class="description">
				<?php
				echo wp_kses(
					__( '<a target="_blank" href="https://website-translation.language-tools.ec.europa.eu/web-t-connecting-languages-0_en">WEB-T</a> is a part of European Multilingual Web (EMW) project by European Commission.', 'etranslation-multilingual' ),
					array(
						'a' => array(
							'href'   => array(),
							'target' => array(),
						),
					)
				);
				?>
			</p>
			<p>
				<img class="about-tab-img" alt="EC logotype" src="<?php echo esc_url( ETM_PLUGIN_URL . 'assets/images/ec_logotype.svg' ); ?>" />
				<img class="about-tab-img" alt="WEB-T logotype" id="webt-logotype" src="<?php echo esc_url( ETM_PLUGIN_URL . 'assets/images/webt_logotype.svg' ); ?>" />
			</p>
		</div>
	</div>
</div>
