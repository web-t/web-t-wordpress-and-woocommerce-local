<?php

add_filter( 'etm_register_advanced_settings', 'etm_register_manage_translations_separator', 2 );
/**
 * Registers the 'Manage translations' separator for advanced settings in eTranslation Multilingual.
 *
 * @param array $settings_array An array of advanced settings.
 * @return array Modified array of advanced settings.
 */
function etm_register_manage_translations_separator( $settings_array ) {
	$settings_array[] = array(
		'name'      => 'manage_translations',
		'type'      => 'separator',
		'label'     => esc_html__( 'Manage translations', 'etranslation-multilingual' ),
		'no-border' => true,
	);
	return $settings_array;
}

add_filter( 'etm_register_advanced_settings', 'etm_register_troubleshoot_separator', 5 );
/**
 * Registers the 'Troubleshooting' separator for advanced settings in eTranslation Multilingual.
 *
 * @param array $settings_array An array of advanced settings.
 * @return array Modified array of advanced settings.
 */
function etm_register_troubleshoot_separator( $settings_array ) {
	$settings_array[] = array(
		'name'      => 'troubleshoot_options',
		'type'      => 'separator',
		'label'     => esc_html__( 'Troubleshooting', 'etranslation-multilingual' ),
	);
	return $settings_array;
}

add_filter( 'etm_register_advanced_settings', 'etm_register_exclude_separator', 95 );
/**
 * Registers the 'Exclude strings' separator for advanced settings in eTranslation Multilingual.
 *
 * @param array $settings_array An array of advanced settings.
 * @return array Modified array of advanced settings.
 */
function etm_register_exclude_separator( $settings_array ) {
	$settings_array[] = array(
		'name'  => 'exclude_strings',
		'type'  => 'separator',
		'label' => esc_html__( 'Exclude strings', 'etranslation-multilingual' ),
	);
	return $settings_array;
}

add_filter( 'etm_register_advanced_settings', 'etm_register_seo_separator', 300 );
/**
 * Registers the 'Additional SEO meta tags' separator for advanced settings in eTranslation Multilingual.
 *
 * @param array $settings_array An array of advanced settings.
 * @return array Modified array of advanced settings.
 */
function etm_register_seo_separator( $settings_array ) {
	$settings_array[] = array(
		'name'  => 'seo_meta_tags',
		'type'  => 'separator',
		'label' => esc_html__( 'Additional SEO meta tags', 'etranslation-multilingual' ),
	);
	return $settings_array;
}

add_filter( 'etm_register_advanced_settings', 'etm_register_debug_separator', 500 );
/**
 * Registers the 'Debug' separator for advanced settings in eTranslation Multilingual.
 *
 * @param array $settings_array An array of advanced settings.
 * @return array Modified array of advanced settings.
 */
function etm_register_debug_separator( $settings_array ) {
	$settings_array[] = array(
		'name'  => 'debug_options',
		'type'  => 'separator',
		'label' => esc_html__( 'Debug', 'etranslation-multilingual' ),
	);
	return $settings_array;
}

add_filter( 'etm_register_advanced_settings', 'etm_register_miscellaneous_separator', 1000 );
/**
 * Registers the 'Miscellaneous options' separator for advanced settings in eTranslation Multilingual.
 *
 * @param array $settings_array An array of advanced settings.
 * @return array Modified array of advanced settings.
 */
function etm_register_miscellaneous_separator( $settings_array ) {
	$settings_array[] = array(
		'name'  => 'miscellaneous_options',
		'type'  => 'separator',
		'label' => esc_html__( 'Miscellaneous options', 'etranslation-multilingual' ),
	);
	return $settings_array;
}

add_filter( 'etm_register_advanced_settings', 'etm_register_custom_language_separator', 2000 );
/**
 * Registers the 'Custom language' separator for advanced settings in eTranslation Multilingual.
 *
 * @param array $settings_array An array of advanced settings.
 * @return array Modified array of advanced settings.
 */
function etm_register_custom_language_separator( $settings_array ) {
	$settings_array[] = array(
		'name'  => 'custom_language',
		'type'  => 'separator',
		'label' => esc_html__( 'Custom language', 'etranslation-multilingual' ),
	);
	return $settings_array;
}
