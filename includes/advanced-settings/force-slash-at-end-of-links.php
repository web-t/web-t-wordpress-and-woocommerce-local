<?php

add_filter( 'etm_register_advanced_settings', 'etm_register_force_slash_in_home_url', 1071 );
/**
 * Register advanced configuration option to force slash at the end of home_url() function.
 *
 * @param array $settings_array The array of settings to add.
 * @return array The updated array of settings.
 */
function etm_register_force_slash_in_home_url( $settings_array ) {
	$settings_array[] = array(
		'name'        => 'force_slash_at_end_of_links',
		'type'        => 'checkbox',
		'label'       => esc_html__( 'Force slash at end of home URL:', 'etranslation-multilingual' ),
		'description' => wp_kses( __( 'Adds a slash at the end of the home_url() function.', 'etranslation-multilingual' ), array( 'br' => array() ) ),
	);
	return $settings_array;
}
