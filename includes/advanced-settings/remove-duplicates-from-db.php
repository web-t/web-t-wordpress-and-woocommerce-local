<?php
add_filter( 'etm_register_advanced_settings', 'etm_register_remove_duplicate_entries_from_db', 530 );
/**
 * Register advanced configuration option to optimize eTranslation Multilingual database tables.
 *
 * @param array $settings_array The array of settings to add.
 * @return array The updated array of settings.
 */
function etm_register_remove_duplicate_entries_from_db( $settings_array ) {
	$settings_array[] = array(
		'name'        => 'remove_duplicate_entries_from_db',
		'type'        => 'text',
		'label'       => esc_html__( 'Optimize WEB-T – eTranslation Multilingual database tables', 'etranslation-multilingual' ),
		'description' => wp_kses_post( sprintf( __( 'You can optimize database tables by <a href="%s">removing duplicate rows from the database</a>.', 'etranslation-multilingual' ), admin_url( 'admin.php?page=etm_remove_duplicate_rows' ) ) ),
	);
	return $settings_array;
}
