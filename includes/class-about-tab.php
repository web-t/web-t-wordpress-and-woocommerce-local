<?php

/**
 * Class ETM_About_Tab
 *
 * This class handles the "About" tab functionality in the eTranslation Multilingual settings.
 */
class ETM_About_Tab {

	/**
	 * Add new tab to ETM settings navigation.
	 *
	 * Hooked to etm_settings_tabs action.
	 *
	 * @param array $tabs An array containing existing tabs in the ETM settings.
	 *
	 * @return array The updated array of tabs with the new "About" tab added.
	 */
	public function add_tab_to_navigation( $tabs ) {
		$tabs[] = array(
			'name' => __( 'About', 'etranslation-multilingual' ),
			'url'  => admin_url( 'admin.php?page=etm_about' ),
			'page' => 'etm_about',
		);

		return $tabs;
	}

	/**
	 * Add submenu page for the "About" tab under the "ETMHidden" parent menu.
	 *
	 * Hooked to admin_menu action.
	 */
	public function add_submenu_page() {
		add_submenu_page(
			'ETMHidden',
			'WEB-T – eTranslation Multilingual About',
			'ETMHidden',
			apply_filters( 'etm_settings_capability', 'manage_options' ),
			'etm_about',
			array( $this, 'about_page_content' )
		);
	}

	/**
	 * Display the content of the "About" page in the ETM settings.
	 */
	public function about_page_content() {
		require_once ETM_PLUGIN_DIR . 'partials/etm-about-page.php';
	}
}
