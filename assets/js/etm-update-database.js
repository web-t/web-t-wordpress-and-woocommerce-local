/**
 * jQuery code to trigger the update of the eTranslation Multilingual database via AJAX.
 */
jQuery( function() {
    /**
     * Function trigger_update_by_ajax
     * 
     * Initiates the database update by sending an AJAX request.
     * Recursively triggers the update until it is completed.
     * 
     * @param {Object} data - The data to be sent in the AJAX request.
     */
    function trigger_update_by_ajax( data ) {
        jQuery.ajax({
            url: etm_updb_localized['admin_ajax_url'],
            type: 'post',
            dataType: 'json',
            data: data,
            success: function (response) {
                jQuery('#etm-update-database-progress').append(response['progress_message']);

                // If the update is not completed, trigger the update again.
                if ( response['etm_update_completed'] == 'no' ) {
                    trigger_update_by_ajax(response);
                }
            },
            error: function (errorThrown) {
                jQuery('#etm-update-database-progress').append(errorThrown['responseText']);
                console.log('WEB-T – eTranslation Multilingual AJAX Request Error while triggering database update');
            }
        });
    };

    // Initial trigger to start the database update via AJAX.
    trigger_update_by_ajax( {
        action: 'etm_update_database',
        etm_updb_nonce: etm_updb_localized['nonce'],
        initiate_update: true,
    } );
});