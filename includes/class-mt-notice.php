<?php

/**
 * Class ETM_MT_Notice
 *
 * Generates MT notice informing user about machine translated content.
 */
class ETM_MT_Notice {

	/**
	 * An array containing various settings for the class instance.
	 *
	 * @var array $settings
	 */
	protected $settings;

	/**
	 * An instance of a URL converter used in the class.
	 *
	 * @var mixed $url_converter
	 */
	protected $url_converter;

	/**
	 * Constructor to initialize the class instance with settings and URL converter.
	 *
	 * @param array $settings      An array containing various settings for the class instance.
	 * @param mixed $url_converter An instance of a URL converter used in the class.
	 */
	public function __construct( $settings, $url_converter ) {
		$this->settings      = $settings;
		$this->url_converter = $url_converter;
	}

	/**
	 * Enqueues scripts and styles for the machine translation notice based on settings.
	 *
	 * This function checks if the machine translation notice should be displayed based on settings.
	 * If the notice is enabled and the current language is different from the default language, it enqueues
	 * the necessary scripts and styles for the notice.
	 */
	public function enqueue_mt_notice_scripts() {
		$show       = $this->settings['etm_machine_translation_settings']['show-mt-notice'];
		$mt_enabled = $this->settings['etm_machine_translation_settings']['machine-translation'] ?? false;
		if ( $show == 'yes' && $mt_enabled == 'yes' ) {
			global $ETM_LANGUAGE;
			$default = $this->settings['default-language'];
			if ( $ETM_LANGUAGE != $default ) {
				$etm                = ETM_eTranslation_Multilingual::get_etm_instance();
				$machine_translator = $etm->get_component( 'machine_translator' );
				$mt_available       = $machine_translator->check_languages_availability( array( $default, $ETM_LANGUAGE ) );
				if ( $mt_available ) {
					$original_url = $this->url_converter->get_url_for_language( $default );
					$img_url      = ETM_PLUGIN_URL . 'assets/images/x.svg';

					wp_enqueue_style( 'mt-notice-style', ETM_PLUGIN_URL . 'assets/css/mt-notice.css', array(), ETM_PLUGIN_VERSION );
					wp_enqueue_script( 'mt-notice-script', ETM_PLUGIN_URL . 'assets/js/mt-notice.js', array( 'jquery' ), ETM_PLUGIN_VERSION );
					wp_localize_script(
						'mt-notice-script',
						'mt_notice_params',
						array(
							'original_url' => $original_url,
							'img_url'      => $img_url,
						)
					);
				}
			}
		}
	}
}
