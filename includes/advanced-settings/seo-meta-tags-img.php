<?php

add_filter( 'etm_register_advanced_settings', 'etm_register_seo_meta_tags_img', 310 );
/**
 * Register advanced configuration option for SEO meta tags with image type value.
 *
 * @param array $settings_array The array of settings to add.
 * @return array The updated array of settings.
 */
function etm_register_seo_meta_tags_img( $settings_array ) {
	$settings_array[] = array(
		'name'        => 'seo_meta_tags_img',
		'type'        => 'list',
		'columns'     => array(
			'name'      => __( 'Meta tag name', 'etranslation-multilingual' ),
			'attribute' => __( 'Meta tag attribute', 'etranslation-multilingual' ),
		),
		'label'       => esc_html__( 'Meta tags with image type value', 'etranslation-multilingual' ),
		'description' => wp_kses( __( 'Add translatable SEO meta tags which contain image type value.', 'etranslation-multilingual' ), array() ),
	);
	return $settings_array;
}
