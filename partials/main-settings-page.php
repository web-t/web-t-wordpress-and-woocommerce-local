<div id="etm-main-settings" class="wrap">
	<form method="post" action="options.php">
		<?php settings_fields( 'etm_settings' ); ?>
		<h1 id="tab_title">
			<img id="tab_title__logo" alt="WEB-T logo" />
			<?php esc_html_e( 'WEB-T – eTranslation Multilingual Settings', 'etranslation-multilingual' ); ?>
		</h1>
		<?php do_action( 'etm_settings_navigation_tabs' ); ?>

		<div id="etm-main-settings__wrap">
			<table id="etm-options" class="form-table">
				<tr>
					<th scope="row" id="etm-default-language-label"><?php esc_html_e( 'Default Language', 'etranslation-multilingual' ); ?> </th>
					<td>
						<select id="etm-default-language" aria-labelledby="etm-default-language-label" name="etm_settings[default-language]" class="etm-select2">
							<?php
							foreach ( $languages as $language_code => $language_name ) {
								?>
								<option title="<?php echo esc_attr( $language_code ); ?>" value="<?php echo esc_attr( $language_code ); ?>" <?php echo ( $this->settings['default-language'] == $language_code ? 'selected' : '' ); ?> >
									<?php echo esc_html( $language_name ); ?>
								</option>
							<?php } ?>
						</select>
						<p class="description">
							<?php esc_html_e( 'Select the original language of your content.', 'etranslation-multilingual' ); ?>
						</p>

						<p class="warning" style="display: none;" >
							<?php esc_html_e( 'WARNING. Changing the default language will invalidate existing translations.', 'etranslation-multilingual' ); ?><br/>
							<?php esc_html_e( 'Even changing from en_GB to en_US, because they are treated as two different languages.', 'etranslation-multilingual' ); ?><br/>
							<?php esc_html_e( 'In most cases changing the default flag is all it is needed: ', 'etranslation-multilingual' ); ?>
						</p>

					</td>
				</tr>

				<?php do_action( 'etm_language_selector', $languages ); ?>

				<tr>
					<th scope="row" id="etm-native-language-name-label"><?php esc_html_e( 'Native language name', 'etranslation-multilingual' ); ?> </th>
					<td>
						<select id="etm-native-language-name" aria-labelledby="etm-native-language-name-label" name="etm_settings[native_or_english_name]" class="etm-select">
							<option value="english_name" <?php selected( $this->settings['native_or_english_name'], 'english_name' ); ?>><?php esc_html_e( 'No', 'etranslation-multilingual' ); ?></option>
							<option value="native_name" <?php selected( $this->settings['native_or_english_name'], 'native_name' ); ?>><?php esc_html_e( 'Yes', 'etranslation-multilingual' ); ?></option>
						</select>
						<p class="description">
							<?php esc_html_e( 'Select Yes if you want to display languages in their native names. Otherwise, languages will be displayed in English.', 'etranslation-multilingual' ); ?>
						</p>
					</td>
				</tr>

				<tr>
					<th scope="row" id="etm-subdirectory-for-default-language-label"><?php esc_html_e( 'Use a subdirectory for the default language', 'etranslation-multilingual' ); ?> </th>
					<td>
						<select id="etm-subdirectory-for-default-language" aria-labelledby="etm-subdirectory-for-default-language-label" name="etm_settings[add-subdirectory-to-default-language]" class="etm-select">
							<option value="no" <?php selected( $this->settings['add-subdirectory-to-default-language'], 'no' ); ?>><?php esc_html_e( 'No', 'etranslation-multilingual' ); ?></option>
							<option value="yes" <?php selected( $this->settings['add-subdirectory-to-default-language'], 'yes' ); ?>><?php esc_html_e( 'Yes', 'etranslation-multilingual' ); ?></option>
						</select>
						<p class="description">
							<?php echo wp_kses( __( 'Select Yes if you want to add the subdirectory in the URL for the default language.</br>By selecting Yes, the default language seen by website visitors will become the first one in the "All Languages" list.', 'etranslation-multilingual' ), array( 'br' => array() ) ); ?>
						</p>
					</td>
				</tr>

				<tr>
					<th scope="row" id="etm-force-language-in-custom-links-label"><?php esc_html_e( 'Force language in custom links', 'etranslation-multilingual' ); ?> </th>
					<td>
						<select id="etm-force-language-in-custom-links" aria-labelledby="etm-force-language-in-custom-links-label" name="etm_settings[force-language-to-custom-links]" class="etm-select">
							<option value="no" <?php selected( $this->settings['force-language-to-custom-links'], 'no' ); ?>><?php esc_html_e( 'No', 'etranslation-multilingual' ); ?></option>
							<option value="yes" <?php selected( $this->settings['force-language-to-custom-links'], 'yes' ); ?>><?php esc_html_e( 'Yes', 'etranslation-multilingual' ); ?></option>
						</select>
						<p class="description">
							<?php esc_html_e( 'Select Yes if you want to force custom links without language encoding to keep the currently selected language.', 'etranslation-multilingual' ); ?>
						</p>
					</td>
				</tr>

				<tr>
					<th scope="row"><?php esc_html_e( 'Language Switcher', 'etranslation-multilingual' ); ?> </th>
					<td>
						<div class="etm-ls-type">
							<label><input type="checkbox" disabled checked id="etm-ls-shortcode" ><b><?php esc_html_e( 'Shortcode ', 'etranslation-multilingual' ); ?>[language-switcher] </b></label>
							<div aria-labelledby="etm-ls-shortcode">
								<?php $this->output_language_switcher_select( 'shortcode-options', $this->settings['shortcode-options'] ); ?>
							</div>
							<p class="description">
								<?php esc_html_e( 'Use shortcode on any page or widget.', 'etranslation-multilingual' ); ?>
							</p>
						</div>
						<div class="etm-ls-type">
							<label><input type="checkbox" id="etm-ls-menu" disabled checked ><b><?php esc_html_e( 'Menu item', 'etranslation-multilingual' ); ?></b></label>
							<div aria-labelledby="etm-ls-menu">
								<?php $this->output_language_switcher_select( 'menu-options', $this->settings['menu-options'] ); ?>
							</div>
							<p class="description">
								<?php
								printf( wp_kses( __( 'Go to  %1$s Appearance -> Menus%2$s to add languages to the Language Switcher in any menu.', 'etranslation-multilingual' ), array( 'a' => array( 'href' => array() ) ) ), '<a href="' . esc_url( admin_url( 'nav-menus.php' ) ) . '">', '</a>' );
								?>
							</p>
						</div>
						<div class="etm-ls-type">
							<label><input type="checkbox" id="etm-ls-floater" name="etm_settings[etm-ls-floater]"  value="yes"  
							<?php
							if ( isset( $this->settings['etm-ls-floater'] ) && ( $this->settings['etm-ls-floater'] == 'yes' ) ) {
								echo 'checked'; }
							?>
							><b><?php esc_html_e( 'Floating language selection', 'etranslation-multilingual' ); ?></b></label>
							<div aria-labelledby="etm-ls-floater">
								<?php $this->output_language_switcher_select( 'floater-options', $this->settings['floater-options'] ); ?>
								<?php $this->output_language_switcher_floater_color( $this->settings['floater-color'] ); ?>
								<?php $this->output_language_switcher_floater_possition( $this->settings['floater-position'] ); ?>
							</div>
							<p class="description">
								<?php esc_html_e( 'Add a floating dropdown that follows the user on every page.', 'etranslation-multilingual' ); ?>
							</p>
						</div>
					</td>
				</tr>

				<?php do_action( 'etm_extra_settings', $this->settings ); ?>
			</table>
		</div>

		<p class="submit"><input type="submit" class="button-primary" value="<?php esc_attr_e( 'Save Changes', 'etranslation-multilingual' ); ?>" /></p>
	</form>
</div>
