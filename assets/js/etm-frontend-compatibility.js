/**
 * JavaScript code to clear WooCommerce cart fragments when switching language using the eTranslation Multilingual plugin.
 */
document.addEventListener("DOMContentLoaded", function(event) {

    /**
     * Function etmClearWooCartFragments
     * 
     * Clears WooCommerce cart fragments when a language is switched using the eTranslation Multilingual plugin.
     */
    function etmClearWooCartFragments() {
        // Get all language switcher URLs that are not disabled.
        var etm_language_switcher_urls = document.querySelectorAll(".etm-language-switcher-container a:not(.etm-ls-disabled-language)");

        for (i = 0; i < etm_language_switcher_urls.length; i++) {
            // Add click event listener to each language switcher URL.
            etm_language_switcher_urls[i].addEventListener("click", function() {
                // Check if wc_cart_fragments_params and fragment_name are defined.
                if (typeof wc_cart_fragments_params !== 'undefined' && typeof wc_cart_fragments_params.fragment_name !== 'undefined') {
                    // Remove the cart fragment from the session storage.
                    window.sessionStorage.removeItem(wc_cart_fragments_params.fragment_name);
                }
            });
        }
    }

    // Call the function to clear WooCommerce cart fragments when language is switched.
    etmClearWooCartFragments();
});