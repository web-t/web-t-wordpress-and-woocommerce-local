<?php

add_filter( 'etm_register_advanced_settings', 'etm_translation_for_gettext_strings', 523 );
/**
 * Register advanced settings for eTranslation Multilingual related to gettext strings.
 *
 * @param array $settings_array The array of settings to add.
 * @return array The updated array of settings.
 */
function etm_translation_for_gettext_strings( $settings_array ) {
	$settings_array[] = array(
		'name'        => 'disable_translation_for_gettext_strings',
		'type'        => 'checkbox',
		'label'       => esc_html__( 'Disable translation for gettext strings', 'etranslation-multilingual' ),
		'description' => wp_kses( __( 'Gettext Strings are strings outputted by themes and plugins. <br> Translating these types of strings through WEB-T – eTranslation Multilingual can be unnecessary if they are already translated using the .po/.mo translation file system.<br>Enabling this option can improve the page load performance of your site in certain cases. The disadvantage is that you can no longer edit gettext translations using WEB-T – eTranslation Multilingual, nor benefit from automated translation on these strings.', 'etranslation-multilingual' ), array( 'br' => array() ) ),
	);
	return $settings_array;
}

add_action( 'etm_before_running_hooks', 'etm_remove_hooks_to_disable_gettext_translation', 10, 1 );
/**
 * Action to remove hooks and disable gettext translation based on advanced settings.
 *
 * @param ETM_eTranslation_Loader $etm_loader The eTranslation Multilingual Loader instance.
 */
function etm_remove_hooks_to_disable_gettext_translation( $etm_loader ) {
	$option = get_option( 'etm_advanced_settings', true );
	if ( isset( $option['disable_translation_for_gettext_strings'] ) && $option['disable_translation_for_gettext_strings'] === 'yes' ) {
		$etm             = ETM_eTranslation_Multilingual::get_etm_instance();
		$gettext_manager = $etm->get_component( 'gettext_manager' );
		$etm_loader->remove_hook( 'init', 'create_gettext_translated_global', $gettext_manager );
		$etm_loader->remove_hook( 'shutdown', 'machine_translate_gettext', $gettext_manager );
	}
}

add_filter( 'etm_skip_gettext_querying', 'etm_skip_gettext_querying', 10, 4 );
/**
 * Filter callback to skip gettext querying based on advanced settings.
 *
 * @param bool   $skip        The current status of skipping gettext querying.
 * @param string $translation The translated text.
 * @param string $text        The original text.
 * @param string $domain      The gettext text domain.
 * @return bool The updated status of skipping gettext querying.
 */
function etm_skip_gettext_querying( $skip, $translation, $text, $domain ) {
	$option = get_option( 'etm_advanced_settings', true );
	if ( isset( $option['disable_translation_for_gettext_strings'] ) && $option['disable_translation_for_gettext_strings'] === 'yes' ) {
		return true;
	}
	return $skip;
}

add_action( 'etm_editor_notices', 'etm_display_message_for_disable_gettext_in_editor', 10, 1 );
/**
 * Action to display a message in the eTranslation Multilingual editor for disabling gettext translation.
 *
 * @param string $etm_editor_notices The existing editor notices.
 * @return string The updated editor notices with the message for disabling gettext translation.
 */
function etm_display_message_for_disable_gettext_in_editor( $etm_editor_notices ) {
	$option = get_option( 'etm_advanced_settings', true );
	if ( isset( $option['disable_translation_for_gettext_strings'] ) && $option['disable_translation_for_gettext_strings'] === 'yes' ) {

		$url = add_query_arg(
			array(
				'page' => 'etm_advanced_page#debug_options',
			),
			site_url( 'wp-admin/admin.php' )
		);

		// maybe change notice color to blue #28B1FF
		$html  = "<div class='etm-notice etm-notice-warning'>";
		$html .= '<p><strong>' . esc_html__( 'Gettext Strings translation is disabled', 'etranslation-multilingual' ) . '</strong></p>';

		$html .= '<p>' . esc_html__( 'To enable it go to ', 'etranslation-multilingual' ) . '<a class="etm-link-primary" target="_blank" href="' . esc_url( $url ) . '">' . esc_html__( 'WEB-T – eTranslation Multilingual->Advanced Settings->Debug->Disable translation for gettext strings', 'etranslation-multilingual' ) . '</a>' . esc_html__( ' and uncheck the Checkbox.', 'etranslation-multilingual' ) . '</p>';
		$html .= '</div>';

		$etm_editor_notices = $html;
	}

	return $etm_editor_notices;
}
