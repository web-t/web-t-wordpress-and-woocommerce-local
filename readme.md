# WEB-T WordPress plugin 
Website translation solution for WordPress websites. 
The WEB-T WordPress plugin allows full website content translation in any language supported by selected MT provider.

## Features
- All website content translation (main content, UI elements, images, dynamic elements etc.)
- Automated translation via eTranslation or other MT provider
- Translation editing, visual editor
- Translation memory and caching
- Configurable language switcher


## Setup
1. Upload ZIP file from `Wordpress dashboard -> Plugins -> Add new -> Upload Plugin -> Choose file -> Install Now`
1.1.	Alternatively, upload unzipped etranslation-multilingual folder to the `/wp-content/plugins/` directory
2.	Activate installed plugin through `Plugins` page
3.	Configure website source & target languages, MT provider credentials for automated translation from plugin settings (`Settings -> WEB-T – eTranslation Multilingual`)

## Data
This plugin stores its settings and translations inside user's WordPress database. In order to prevent accidental loss of data, this data is not deleted upon plugin uninstallation.

**Tables created by this plugin:**
- wp_etm_dictionary* – the dictionary tables contain manually or automatedly translated strings
- wp_etm_gettext* – the gettext tables contain theme/plugin strings that already have a translation
- wp_etm_original_strings – the original strings table contains strings in the default language, without any translation
- wp_etm_original_meta – the original meta table contains meta information which refers to the post parent’s id

**Plugin settings in WordPress tables:**
- \_postmeta table – meta keys inserted into the \_postmeta table have the \_etm\_ prefix
- \_termmeta table – meta keys inserted into the \_termmeta table have the \_etm\_ prefix
- \_options table – options inserted into the \_options table have the etm\_ prefix
- \_usermeta table – meta keys inserted into the \_usermeta table have the \_etm\_ prefix

## Dependencies
This plugin is a fork of TranslatePress Multilingual which includes following libraries:
- PHP Simple HTML DOM Parser 1.9.1
- Select2 4.0.13
- Vue 2.7.14
- autosize 4.0.4
- is-buffer 2.0.3
- axios 0.18.1
- he 1.2.0
- Simple Diff 0.1.1
- string-similarity 3.0.0
- Translation template extractor 7.x-1.0


## License
This plugin uses GPLv2 license.