﻿=== WEB-T – eTranslation Multilingual ===
Contributors: europeancommission, tildesia, cozmoslabs, razvan.mo, madalin.ungureanu, sareiodata, cristophor
Tags: translate, translation, multilingual, automatic translation, localization
Requires at least: 3.1.0
Tested up to: 6.6.2
Requires PHP: 5.6.20
Stable tag: 2.0.5
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Make your site multilingual in few steps with WEB-T – eTranslation Multilingual WordPress plugin. 

== Description ==

Creating multilingual websites has never been easier: the [WEB-T – eTranslation Multilingual](https://website-translation.language-tools.ec.europa.eu/solutions/web-t-wordpress_en) WordPress plug-in can automatedly translate your web content into 30+ languages – free, easy and secure as always.
WEB-T – eTranslation Multilingual allows full website content translation in any language supported by the eTranslation automated translation service provided by the EC. The automated translations are performed by [eTranslation service API](https://commission.europa.eu/resources-partners/etranslation_en).

== Features ==

* Translates your web content automatedly
* Easy to use
* East to integrate
* Visual translation editor
* Automated translation in 30+ languages via eTranslation integration
* Image translation support
* Translation caching for quick page load times
* Dynamic content translation
* Compatible with WooCommerce, Gutenberg and other plugins & themes
* Configurable language switcher

== Automated translation ==

WEB-T enables seamless translation of your website's entire content into any language supported by compatible automated translation services. You can integrate your preferred service using APIs or opt for the free eTranslation service provided by the European Commission (EC).
While WEB-T itself is free, access to the eTranslation service and API key is limited to organizations and individuals within the European Union (EU) and its associated countries. 
If you're located outside the EU and require translation services, you'll need to find a machine translation (MT) service provider compatible with the WEB-T API. Instructions on how to ensure compatibility with WEB-T APIs can be found [here](https://website-translation.language-tools.ec.europa.eu/expanding-web-t-new-machine-translation_en).

== Installation ==

1. Upload etranslation-multilingual.zip file from Wordpress dashboard -> Plugins -> Add new -> Upload Plugin -> Choose file -> Install Now
1.1.	Alternatively, upload unzipped etranslation-multilingual folder to the ‘/wp-content/plugins/’ directory
2.	Activate installed plugin through ‘Plugins’ menu (Plugins -> WEB-T – eTranslation Multilingual -> Activate)
3.	Configure website source & target languages, eTranslation credentials for automated translation from plugin settings (Settings -> WEB-T – eTranslation Multilingual)

Detailed setup & usage tutorials and videos are available [here](https://website-translation.language-tools.ec.europa.eu/instruction-information-about-wordpress-woocommerce_en).


== Screenshots ==

1. WEB-T – eTranslation Multilingual language and domain configuration
2. Automated translation configuration with eTranslation credentials
3. Floating Language Switcher
4. Selecting page language
5. Translated page, machine-translated content notice
6. Visual translation editor

== More information ==

See how to configure & use this plugin and read more about the WEB-T project by European Commission on official [WEB-T website](https://website-translation.language-tools.ec.europa.eu/).

== Credits ==

WEB-T – eTranslation Multilingual is a fork of [TranslatePress](https://translatepress.com) by [Cozmoslabs](https://www.cozmoslabs.com). We thank TranslatePress developers (Cozmoslabs, Razvan Mocanu, Madalin Ungureanu, Cristophor Hurduban) for their work on the original plugin.

== Changelog ==

= 2.0.5 =
* Resolved eTranslation connection issue with OpenSSL 3.0 & cURL versions < 7.88.1
= 2.0.4 =
* Support for WooCommerce High-Performance Order Storage (HPOS)
= 2.0.3 =
* Plugin description improvements
= 2.0.2 =
* Accessibility improvements
* Pre-translation URL load fix
= 2.0.1 =
* Fixed invalid domain sent in MT request
* Small bugfixes related to 'Translate Site' tab
= 2.0.0 =
* Renamed to 'WEB-T – eTranslation Multilingual'
* Custom MT provider support
* SEO support
= 1.0.0 =
* Initial release.
