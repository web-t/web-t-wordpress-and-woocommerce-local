<?php
add_filter( 'etm_register_advanced_settings', 'etm_register_pretranslate', 3 );
/**
 * Register the "Translate all" setting in the Advanced section.
 *
 * @param array $settings_array The array of existing settings.
 * @return array The updated array of settings with the "Translate all" setting added.
 */
function etm_register_pretranslate( $settings_array ) {
	$settings_array[] = array(
		'name'        => 'pretranslate',
		'type'        => 'button',
		'label'       => esc_html__( 'Translate all', 'etranslation-multilingual' ),
		'description' => wp_kses( __( 'Pre-translate all website pages and posts. Only loads translatable pages, without checking translation status.' ), array() ),
	);
	return $settings_array;
}