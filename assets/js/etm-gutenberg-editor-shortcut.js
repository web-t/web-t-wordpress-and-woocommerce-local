/**
 * jQuery code to place the eTranslation Multilingual button in the Gutenberg editor.
 */
jQuery( function() {
    /**
     * Function place_etm_button
     * 
     * Checks if the Gutenberg editor root element is present and places the eTranslation
     * Multilingual button in the editor toolbar if it is available.
     */
    function place_etm_button() {
        // Check if Gutenberg's editor root element is present.
        var editorEl = document.getElementById( 'editor' );
        if ( !editorEl ) { 
            // Do nothing if there's no Gutenberg root element on the page.
            return;
        }

        // Subscribe to WordPress data to detect changes in the editor.
        var unsubscribe = wp.data.subscribe( function () {
            if ( !document.getElementById( "etm-link-id" ) ) {
                // Check if the eTranslation Multilingual button is not already present.
                var toolbalEl = editorEl.querySelector( '.edit-post-header-toolbar__left' );
                if ( toolbalEl instanceof HTMLElement ) {
                    // Insert the eTranslation Multilingual button after the toolbar element.
                    toolbalEl.insertAdjacentHTML("afterend", etm_url_etm_editor[0]);
                }
            }
        });
    }

    // Call the function to place the eTranslation Multilingual button.
    place_etm_button();
});