<div id="etm-addons-page" class="wrap">

    <h1> <?php esc_html_e( 'WEB-T – eTranslation Multilingual Database Updater', 'etranslation-multilingual' );?></h1>

    <div class="grid feat-header">
        <div class="grid-cell">
            <h2><?php esc_html_e('Updating WEB-T – eTranslation Multilingual tables', 'etranslation-multilingual' );?> </h2>
	        <div id="etm-update-database-progress">

            </div>
        </div>
    </div>

</div>