<?php

add_filter( 'etm_register_advanced_settings', 'etm_etranslation_use_curl_client', 71 );
/**
 * Register the setting to use cURL client for eTranslation requests.
 *
 * @param array $settings_array The array of advanced settings.
 * @return array Modified array of advanced settings with the new option.
 */
function etm_etranslation_use_curl_client( $settings_array ) {
	$settings_array[] = array(
		'name'        => 'etranslation_use_curl_client',
		'type'        => 'checkbox',
		'label'       => esc_html__( 'Use cURL client for eTranslation HTTP requests', 'etranslation-multilingual' ),
		'description' => wp_kses( 'Fixes SSL issues between WordPress HTTP API and eTranslation for certain cURL versions.', 'etranslation-multilingual' ),
	);
	return $settings_array;
}
