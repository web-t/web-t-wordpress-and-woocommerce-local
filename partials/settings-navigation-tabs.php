<div class="nav-tab-wrapper">
		<?php
		$etm      = ETM_eTranslation_Multilingual::get_etm_instance();
		$settings = $etm->get_component( 'settings' );
		foreach ( $tabs as $tb ) {
			$id_str = '';
			if ( $tb['page'] == 'etranslation-multilingual' && ! $settings->mt_setup_done() ) {
				$id_str = 'show-login-alert';
			}
			echo '<h2><a href="' . esc_url( $tb['url'] ) . '" id="' . esc_attr( $id_str ) . '" class="nav-tab ' . ( ( $active_tab == $tb['page'] ) ? 'nav-tab-active' : '' ) . '" ' . ( ( $active_tab == $tb['page'] ) ? 'aria-current="page"' : '' ) . '>' . esc_html( $tb['name'] ) . ( $tb['page'] == 'etm_translation_editor' ? '<img class="external-link-icon" src="' . esc_url( ETM_PLUGIN_URL ) . 'assets/images/external-link.svg" alt="External link"/>' : '' ) . '</a></h2>';
		}
		?>
</div>
