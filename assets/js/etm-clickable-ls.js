/**
 * JavaScript code to handle click events and keyup events for eTranslation Multilingual plugin's language switcher shortcode.
 * When the current language or any other language is clicked, the corresponding classes are added to the elements.
 * When the "Escape" key is pressed, the classes are removed.
 * When a click event is detected outside the language switcher, the classes are removed.
 */
jQuery('.etm_language_switcher_shortcode .etm-ls-shortcode-current-language').click(function () {
    jQuery( '.etm_language_switcher_shortcode .etm-ls-shortcode-current-language' ).addClass('etm-ls-clicked');
    jQuery( '.etm_language_switcher_shortcode .etm-ls-shortcode-language' ).addClass('etm-ls-clicked');
});

jQuery('.etm_language_switcher_shortcode .etm-ls-shortcode-language').click(function () {
    jQuery( '.etm_language_switcher_shortcode .etm-ls-shortcode-current-language' ).removeClass('etm-ls-clicked');
    jQuery( '.etm_language_switcher_shortcode .etm-ls-shortcode-language' ).removeClass('etm-ls-clicked');
});

jQuery(document).keyup(function(e) {
    if (e.key === "Escape") {
        jQuery( '.etm_language_switcher_shortcode .etm-ls-shortcode-current-language' ).removeClass('etm-ls-clicked');
        jQuery( '.etm_language_switcher_shortcode .etm-ls-shortcode-language' ).removeClass('etm-ls-clicked');
    }
});

jQuery(document).on("click", function(event){
    if(!jQuery(event.target).closest(".etm_language_switcher_shortcode .etm-ls-shortcode-current-language").length){
        jQuery( '.etm_language_switcher_shortcode .etm-ls-shortcode-current-language' ).removeClass('etm-ls-clicked');
        jQuery( '.etm_language_switcher_shortcode .etm-ls-shortcode-language' ).removeClass('etm-ls-clicked');
    }
});