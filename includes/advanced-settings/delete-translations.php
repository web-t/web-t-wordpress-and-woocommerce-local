<?php

add_filter( 'etm_register_advanced_settings', 'etm_register_delete_translations', 4 );
/**
 * Register the "Delete translations" setting in the Advanced section.
 *
 * @param array $settings_array The array of existing settings.
 * @return array The updated array of settings with the "Delete translations" setting added.
 */
function etm_register_delete_translations( $settings_array ) {
	$settings_array[] = array(
		'name'        => 'delete_translations',
		'type'        => 'button',
		'label'       => esc_html__( 'Delete translations', 'etranslation-multilingual' ),
		'description' => wp_kses( __( 'Remove all translations stored by this plugin from database.', 'etranslation-multilingual' ), array() ),
	);
	return $settings_array;
}
